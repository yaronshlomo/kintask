package com.yshlomo.kintask.retrofit

import com.yshlomo.kintask.retrofit.models.SearchResultModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import android.graphics.ColorSpace.Model
import com.yshlomo.kintask.retrofit.models.MovieDetailModel


interface SearchService {

    @GET("search/movie?/")
    fun searchMovie(
        @Query("api_key") apiKey: String,
        @Query("query") query: String,
        @Query("region") region: String):
            Observable<SearchResultModel>


    @GET("movie/{movie_id}?/")
    fun getMovieDetail(
        @Path("movie_id") movieId: String,
        @Query("api_key") apiKey: String):
            Observable<MovieDetailModel>

}