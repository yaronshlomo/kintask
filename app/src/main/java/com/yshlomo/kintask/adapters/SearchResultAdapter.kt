package com.yshlomo.kintask.adapters

import android.support.v7.widget.RecyclerView
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yshlomo.kintask.retrofit.models.Result
import android.view.LayoutInflater
import com.yshlomo.kintask.R
import com.yshlomo.kintask.listeners.OnMovieClickListener


class SearchResultAdapter(context: Context, data: List<Result>, onMovieClickListener: OnMovieClickListener) :
    RecyclerView.Adapter<SearchResultAdapter.ViewHolder>() {

    private val TAG = SearchResultAdapter::class.java.simpleName
    private val items = data
    private val mInflater: LayoutInflater
    private val mContext = context
    private val mMovieClickListener = onMovieClickListener

    init {
        mInflater = LayoutInflater.from(mContext);
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.movie_name, parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.setText(items.get(position).title)
        holder.textView.setTag(R.string.abc_capital_on, items.get(position))
    }

    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        internal var textView: TextView

        init {
            textView = itemView as TextView
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            mMovieClickListener.onMovieClick(itemView.getTag(R.string.abc_capital_on) as Result)
        }
    }
}