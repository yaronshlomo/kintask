package com.yshlomo.kintask

import android.app.Activity
import android.app.Application
import com.yshlomo.kintask.injection.components.AppComponent
import com.yshlomo.kintask.injection.components.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class KinTaskApplication: Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): DispatchingAndroidInjector<Activity>? {
        return dispatchingAndroidInjector
    }

//    lateinit var mComponent : AppComponent
//
//    fun getAppComponent() : AppComponent = mComponent

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
            .application(this)
            .context(applicationContext)
            .build()
            .inject(this)
    }

 /*   private fun initDagger() {
        mComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()

        mComponent.repositoriesBuilder().build()
    }
*/

}