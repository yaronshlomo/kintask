package com.yshlomo.kintask.models

import android.os.Bundle
import com.yshlomo.kintask.adapters.SearchResultAdapter
import com.yshlomo.kintask.retrofit.models.MovieDetailModel

class DisplayResultModel(var searchResultAdapter: SearchResultAdapter? = null,
                         var replaceAdapter: Boolean = false,
                         var showDetailFragment: Boolean = false,
                         var movieDetailBundle: Bundle? = null,
                         var movieDetailModel: MovieDetailModel? = null,
                         var closeFragment: Boolean = false,
                         var closeKeyboard: Boolean = false,
                         var showError: Boolean = false,
                         var errorMessage: String = "")
