package com.yshlomo.kintask.injection.components

import android.app.Application
import android.content.Context
import com.an.dagger.di.module.ViewModelModule
import com.yshlomo.kintask.KinTaskApplication
import com.yshlomo.kintask.injection.modules.ActivityModule
import com.yshlomo.kintask.injection.modules.ApiModule
import com.yshlomo.kintask.injection.modules.RepositoriesModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        ApiModule::class,
        ViewModelModule::class,
        ActivityModule::class,
        RepositoriesModule::class,
        AndroidSupportInjectionModule::class]
)
@Singleton
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }




    /*
     * This is our custom Application class
     * */
    fun inject(appController: KinTaskApplication)
}