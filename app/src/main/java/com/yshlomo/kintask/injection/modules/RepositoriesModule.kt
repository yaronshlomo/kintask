package com.yshlomo.kintask.injection.modules

import android.content.Context
import com.yshlomo.kintask.repositories.SearchRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
class RepositoriesModule {

    @Provides
    @Reusable
    fun provideSearchRepository(context: Context) = SearchRepository(context)
}
