package com.an.dagger.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.yshlomo.kintask.injection.SearchViewModelFactory
import com.yshlomo.kintask.injection.ViewModelKey
import com.yshlomo.kintask.viewmodels.SearchViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: SearchViewModelFactory): ViewModelProvider.Factory


    /*
     * This method basically says
     * inject this object into a Map using the @IntoMap annotation,
     * with the  MovieListViewModel.class as key,
     * and a Provider that will build a MovieListViewModel
     * object.
     *
     * */

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    protected abstract fun movieListViewModel(moviesListViewModel: SearchViewModel): ViewModel
}