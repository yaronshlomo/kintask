/*
 * code:<br />Copyright (C) 2018 SIRIN LABS AG
 */

package com.yshlomo.kintask.injection

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.yshlomo.kintask.repositories.SearchRepository
import com.yshlomo.kintask.viewmodels.SearchViewModel
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SearchViewModelFactory @Inject
constructor(
    private val mApplication: Application,
    private val mSearchRepository: SearchRepository
) : ViewModelProvider.Factory{

    override fun <T : ViewModel> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(SearchViewModel::class.java) -> SearchViewModel(mSearchRepository, mApplication)
                else -> throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T
}