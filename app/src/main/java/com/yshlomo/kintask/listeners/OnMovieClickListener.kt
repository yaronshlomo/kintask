package com.yshlomo.kintask.listeners

import com.yshlomo.kintask.retrofit.models.Result

interface OnMovieClickListener {
    fun onMovieClick(result: Result)
}