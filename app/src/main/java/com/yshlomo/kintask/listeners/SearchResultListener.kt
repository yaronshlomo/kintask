package com.yshlomo.kintask.listeners

import com.yshlomo.kintask.retrofit.models.MovieDetailModel
import com.yshlomo.kintask.retrofit.models.SearchResultModel

interface SearchResultListener {
    fun onSearchResult(searchResult: SearchResultModel)
    fun onMovieDetail(movieDetailModel: MovieDetailModel)
    fun onError(throwable: Throwable?)
}