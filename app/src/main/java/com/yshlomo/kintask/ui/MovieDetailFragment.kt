package com.yshlomo.kintask.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.yshlomo.kintask.R
import kotlinx.android.synthetic.main.movie_detail_fragment.*




class MovieDetailFragment : Fragment() {

    private var mFragmentListener: FragmentListener? = null

    companion object {
        val BUDGET_KEY = "budget_key"
        val REVENUE_KEY = "revenue_key"
        val OVERVIEW_KEY = "overview_key"
        val RELEASE_KEY = "release_key"
        val TITLE_KEY = "title_key"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.movie_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(arguments != null){
            revenue.setText(arguments?.getString(REVENUE_KEY))
            budget.setText(arguments?.getString(BUDGET_KEY))
            release_date.setText(arguments?.getString(RELEASE_KEY))
            overview.setText(arguments?.getString(OVERVIEW_KEY))
            title.setText(arguments?.getString(TITLE_KEY))
        }

        close_button.setOnClickListener { mFragmentListener?.onClose() }
    }

    fun setFragmentListener(fragmentListener: FragmentListener){
        mFragmentListener = fragmentListener
    }

    interface FragmentListener{
        fun onClose()
    }
}