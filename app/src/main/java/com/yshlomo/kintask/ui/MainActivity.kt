package com.yshlomo.kintask.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.jakewharton.rxbinding2.widget.RxTextView
import com.yshlomo.kintask.KinTaskApplication
import com.yshlomo.kintask.R
import com.yshlomo.kintask.models.DisplayResultModel
import com.yshlomo.kintask.viewmodels.SearchViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import android.app.Activity
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Handler
import android.view.inputmethod.InputMethodManager
import android.support.design.widget.Snackbar
import dagger.android.AndroidInjection


class MainActivity : AppCompatActivity(), MovieDetailFragment.FragmentListener {
    private val TAG = MainActivity::class.java.simpleName

    @Inject
    lateinit var mSearchViewModel: SearchViewModel

    private lateinit var mDisposable: Disposable
    private lateinit var mSearchObservable: Observable<Unit>
    private var mFirstTime = true

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory



    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        mSearchViewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)
        mSearchViewModel.mSearchResultLiveData.observe(this, android.arch.lifecycle.Observer {
            if(!mFirstTime){
                onDisplayChange(it!!)
            }
            mFirstTime = false

        })

        mSearchObservable = RxTextView.textChanges(search_edit_text)
            .map { searchText -> mSearchViewModel.searchMovie(searchText.toString()) }

        search_result_list.layoutManager = LinearLayoutManager(this)
    }

    override fun onResume() {
        super.onResume()
        mDisposable = mSearchObservable.subscribe({}, { throwable -> Log.e(TAG, throwable.localizedMessage) })
    }

    override fun onPause() {
        super.onPause()
        mDisposable.dispose()
    }

    override fun onAttachFragment(fragment: Fragment?) {
        if (fragment is MovieDetailFragment) {
            val headlinesFragment = fragment as MovieDetailFragment?
            headlinesFragment!!.setFragmentListener(this)
        }
    }

    override fun onClose() {
        mSearchViewModel.closeMovieDetail()
    }

    private fun onDisplayChange(displayResultModel: DisplayResultModel) {
        if (displayResultModel.replaceAdapter) {
            search_result_list.adapter = displayResultModel.searchResultAdapter
        }

        if (displayResultModel.showDetailFragment) {
            showDetailFragment(displayResultModel)
        }

        if (displayResultModel.closeFragment) {
            closeDetailFragment()
        }

        if (displayResultModel.closeKeyboard) {
            Handler().postDelayed(Runnable { hideKeyboard(this) }, 5)
        }

        if (displayResultModel.showError) {
            val snackbar = Snackbar
                .make(main_layout, displayResultModel.errorMessage, Snackbar.LENGTH_LONG)

            snackbar.show()
        }
    }

    override fun onBackPressed() {
        if (!closeDetailFragment()) {
            super.onBackPressed()
        }

    }

    private fun showDetailFragment(displayResultModel: DisplayResultModel) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()

        movie_detail_container.visibility = View.VISIBLE
        search_edit_text.visibility = View.GONE
        search_result_list.visibility = View.GONE
        val fragment = MovieDetailFragment()
        fragment.arguments = displayResultModel.movieDetailBundle

        fragmentTransaction?.add(
            com.yshlomo.kintask.R.id.movie_detail_container,
            fragment,
            MovieDetailFragment::class.java.simpleName
        )
        fragmentTransaction?.commit()
    }

    private fun closeDetailFragment(): Boolean {
        //remove fragment
        var isClosed = false

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()

        val fragment = fragmentManager?.findFragmentByTag(MovieDetailFragment::class.java.simpleName)
        if (fragment != null) {
            fragmentTransaction?.remove(fragment)?.commit() ?: -1
            isClosed = true
        }

        //show list
        movie_detail_container.visibility = View.GONE
        search_edit_text.visibility = View.VISIBLE
        search_result_list.visibility = View.VISIBLE
        Handler().postDelayed(Runnable { search_edit_text.requestFocus() }, 5)

        return isClosed
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(search_edit_text.windowToken, 0)
    }
}
