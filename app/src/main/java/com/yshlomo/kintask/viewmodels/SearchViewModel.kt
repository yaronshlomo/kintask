package com.yshlomo.kintask.viewmodels

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.os.Bundle
import android.util.Log
import com.yshlomo.kintask.adapters.SearchResultAdapter
import com.yshlomo.kintask.listeners.OnMovieClickListener
import com.yshlomo.kintask.listeners.SearchResultListener
import com.yshlomo.kintask.models.DisplayResultModel
import com.yshlomo.kintask.repositories.SearchRepository
import com.yshlomo.kintask.retrofit.models.MovieDetailModel
import com.yshlomo.kintask.retrofit.models.Result
import com.yshlomo.kintask.retrofit.models.SearchResultModel
import com.yshlomo.kintask.ui.MovieDetailFragment.Companion.BUDGET_KEY
import com.yshlomo.kintask.ui.MovieDetailFragment.Companion.OVERVIEW_KEY
import com.yshlomo.kintask.ui.MovieDetailFragment.Companion.RELEASE_KEY
import com.yshlomo.kintask.ui.MovieDetailFragment.Companion.REVENUE_KEY
import com.yshlomo.kintask.ui.MovieDetailFragment.Companion.TITLE_KEY
import javax.inject.Inject


class SearchViewModel @Inject constructor(searchRepository: SearchRepository, context: Context) : SearchResultListener,
    OnMovieClickListener, ViewModel() {

    private val TAG = SearchViewModel::class.java.simpleName
    private val mSearchRepository = searchRepository
    private val mContext = context
    val mSearchResultLiveData: MediatorLiveData<DisplayResultModel> = MediatorLiveData()
    private var mSearchAdapter: SearchResultAdapter? = null
    private var mDisplayResultModel = DisplayResultModel()

    companion object {
        val API_KEY = "467195c3da99b29936b75720cffb8c6a"
    }

    init {
        mSearchRepository.setSearchResultListener(this)
    }

    override fun onSearchResult(searchResult: SearchResultModel) {
        mDisplayResultModel = DisplayResultModel()
        mSearchAdapter = SearchResultAdapter(mContext, searchResult.results!!, this)
        mDisplayResultModel.searchResultAdapter = mSearchAdapter
        mDisplayResultModel.replaceAdapter = true
        mSearchResultLiveData.postValue(mDisplayResultModel)
    }

    override fun onMovieClick(result: Result) {
        mSearchRepository.searchMovieDetailes(result.id!!)
    }

    override fun onError(throwable: Throwable?) {
        mDisplayResultModel = DisplayResultModel()
        val emptyList = List<Result>(0){ Result() }
        mSearchAdapter = SearchResultAdapter(mContext, emptyList, this)
        mDisplayResultModel.searchResultAdapter = mSearchAdapter
        mDisplayResultModel.replaceAdapter = true
        mDisplayResultModel.errorMessage = throwable!!.localizedMessage
        mDisplayResultModel.showError = true

        mSearchResultLiveData.postValue(mDisplayResultModel)
    }

    override fun onMovieDetail(movieDetailModel: MovieDetailModel) {
        Log.d(TAG, "movieDetailModel: " + movieDetailModel.id)
        mDisplayResultModel = DisplayResultModel()
        mDisplayResultModel.showDetailFragment = true
        val bundle = Bundle()
        bundle.putString(BUDGET_KEY, movieDetailModel.budget.toString())
        bundle.putString(REVENUE_KEY, movieDetailModel.revenue.toString())
        bundle.putString(OVERVIEW_KEY, movieDetailModel.overview.toString())
        bundle.putString(RELEASE_KEY, movieDetailModel.releaseDate.toString())
        bundle.putString(TITLE_KEY, movieDetailModel.title.toString())
        mDisplayResultModel.movieDetailBundle = bundle
        mDisplayResultModel.closeKeyboard = true
        mSearchResultLiveData.postValue(mDisplayResultModel)
    }

    fun searchMovie(queryString: String){
        mSearchRepository.searchMovies(queryString)
    }

    fun closeMovieDetail() {
        mDisplayResultModel.closeFragment = true
        mDisplayResultModel.showDetailFragment = false
        mSearchResultLiveData.postValue(mDisplayResultModel)
    }

}