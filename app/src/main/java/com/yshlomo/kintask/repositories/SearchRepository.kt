package com.yshlomo.kintask.repositories

import android.content.Context
import android.util.Log
import com.yshlomo.kintask.listeners.SearchResultListener
import com.yshlomo.kintask.retrofit.SearchService
import com.yshlomo.kintask.viewmodels.SearchViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory



class SearchRepository(val mContext: Context) {

    private val TAG: String? = SearchRepository::class.java.simpleName
    private val mSearchService: SearchService
    lateinit var mSearchResultListener: SearchResultListener

    init{
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://api.themoviedb.org/3/")
            .build()

        mSearchService = retrofit.create(SearchService::class.java)
    }

    fun setSearchResultListener(searchResultListener: SearchResultListener){
        mSearchResultListener = searchResultListener
    }

    fun searchMovies(queryString: String){
        mSearchService.searchMovie(SearchViewModel.API_KEY, queryString, "US")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({mSearchResultListener.onSearchResult(it)},{throwable-> mSearchResultListener.onError(throwable); Log.e(TAG, throwable.localizedMessage)})
    }

    fun searchMovieDetailes(id: Int) {
        Log.d(TAG, "searchMovieDetailes: " + id)
        mSearchService.getMovieDetail(id.toString(), SearchViewModel.API_KEY)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({mSearchResultListener.onMovieDetail(it)},{error-> Log.e(TAG, error.localizedMessage)})

    }


}